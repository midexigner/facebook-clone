import firebase from "firebase"

// Replace this with your own config details
const firebaseConfig = {
    apiKey: "AIzaSyB5KlyMl3KQykalpvtOJlvoxjFPnyb-zhM",
    authDomain: "facebook-clone-4a5bd.firebaseapp.com",
    databaseURL: "https://facebook-clone-4a5bd.firebaseio.com",
    projectId: "facebook-clone-4a5bd",
    storageBucket: "facebook-clone-4a5bd.appspot.com",
    messagingSenderId: "5031504727",
    appId: "1:5031504727:web:353a08223d671ca8812c12"
  };

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();

export {auth,provider}
export default db;
