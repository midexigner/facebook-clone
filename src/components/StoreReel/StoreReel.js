import React from 'react'
import Story from '../Story/Story'
import './StoreReel.css'

const StoreReel = () => {
    return (
        <div className='storyReel'>
            <Story image='https://cdn.pixabay.com/photo/2020/11/04/19/22/windmill-5713337_960_720.jpg' profileSrc="https://images.generated.photos/0NprK4zyJYvQOEpytSRPpKlpl2S_FfQb6_YwkKCyt80/rs:fit:512:512/Z3M6Ly9nZW5lcmF0/ZWQtcGhvdG9zL3Yz/XzA3ODg3NzJfMDEz/MDQwNV8wNzQ0NjM4/LmpwZw.jpg" title="Olivia" />
            <Story image='https://cdn.pixabay.com/photo/2020/11/04/15/29/coffee-beans-5712780_960_720.jpg' profileSrc="https://images.generated.photos/Jc0VWXvownnnJCRh5q1Z0X5wt1cwesYnKJrdszXyMco/rs:fit:512:512/Z3M6Ly9nZW5lcmF0/ZWQtcGhvdG9zL3Yz/XzA4MzkwOTFfMDQ3/OTM5OV8wODAxODEx/LmpwZw.jpg" title="Emma" />
            <Story image='https://cdn.pixabay.com/photo/2017/07/13/16/07/monkey-2500919_960_720.jpg' profileSrc="https://images.generated.photos/rUyYKyXWxF-KRDU8K3qhiLzeshXlgSMGZi29ZE_5MRo/rs:fit:512:512/Z3M6Ly9nZW5lcmF0/ZWQtcGhvdG9zL3Yz/XzA5OTQyNDNfMDA5/Mzg2MF8wMzk3ODMw/LmpwZw.jpg" title="Ava" />
            <Story image='https://cdn.pixabay.com/photo/2019/03/20/23/03/florence-4069756_960_720.jpg' profileSrc="https://images.generated.photos/qTzbda9m9zEvELDG-hqPymCDgVdT84nPiFQpV1lqrDk/rs:fit:512:512/Z3M6Ly9nZW5lcmF0/ZWQtcGhvdG9zL3Yz/XzAzNTg2MzlfMDg2/MjI0M18wMjM2OTMw/LmpwZw.jpg" title="Sophia" />
            <Story image='https://cdn.pixabay.com/photo/2020/10/24/06/04/stream-5680609_960_720.jpg' profileSrc="https://images.generated.photos/lwIVO4lrR4eTrzmSN2GeL-L6zuL1kU0DDZ2a461gBlg/rs:fit:512:512/Z3M6Ly9nZW5lcmF0/ZWQtcGhvdG9zL3Yz/XzA5NzE2MTdfMDMy/NTcyNV8wODMxMTUx/LmpwZw.jpg" title="Charlotte" />
        </div>
    )
}

export default StoreReel
