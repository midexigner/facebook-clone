import React, { useState,useEffect } from 'react'
import MessageSender from '../MessageSender/MessageSender'
import Post from '../Post/Post'
import StoreReel from '../StoreReel/StoreReel'
import "./Feed.css"
import db from '../../firebase'
import { useStateValue } from '../../StateProvider'

const Feed = () => {
    const [posts, setPosts] = useState([]);
    const [{user},dispatch] = useStateValue();

useEffect(()=>{
db.collection('posts').orderBy('timestamp','desc').onSnapshot(snapshot =>{
    setPosts(snapshot.docs.map(doc=>({
        id:doc.id,data:doc.data()
    })));
})
},[])

    return (
        <div className="feed">
            {/* StoreReel */}
            <StoreReel />
            {/* MessageSender */}
            <MessageSender />
            {/* Post */}
            {posts.map(post =>(
            <Post 
            key={post.data.id}
            profilePic={post.data.profilePic} 
            image={post.data.image} 
            username={post.data.username} 
            timestamp={post.data.timestamp}
            message={post.data.message}  />
           ))}
        </div>
    )
}

export default Feed
