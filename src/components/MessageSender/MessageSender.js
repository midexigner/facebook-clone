import { Avatar } from '@material-ui/core'
import { Videocam,PhotoLibrary, InsertEmoticon } from '@material-ui/icons'
import React, { useState } from 'react'
import { useStateValue } from '../../StateProvider'
import './MessageSender.css'
import firebase from 'firebase'
import db from '../../firebase'

const MessageSender = () => {
    const [input, setInput] = useState('');
    const [imgurl, setImgurl] = useState('');
    const [{user},dispatch] = useStateValue();

    const handleSubmit = (e)=>{
        e.preventDefault();
db.collection('posts').add({
    message:input,
    timestamp:firebase.firestore.FieldValue.serverTimestamp(),
    profilePic:user.photoURL,
    username:user.displayName,
    image:imgurl,
})

        setInput("");
        setImgurl("");
    }
    return (
        <div className='messageSender'>
            <div className="messageSender__top">
                <Avatar src={user.photoURL} />
                <form>
                    <input
                    value={input}
                    onChange={(e)=>setInput(e.target.value)} 
                    type="text" className='messageSender__input' placeholder={`What's on your mind,`} />
                    <input
                    value={imgurl}
                    onChange={(e)=>setImgurl(e.target.value)} 
                     type="text" placeholder="image URL (Optional)" />
                <button onClick={handleSubmit} type="submit">Hidden Submit</button>
                </form>
            </div>
            <div className="messageSender__bottom">
               <div className="messageSender__option">
                <Videocam style={{color:'red'}} />
               <h3>Live Video</h3>
               </div>
               <div className="messageSender__option">
               <PhotoLibrary style={{color:'green'}} />
               <h3>Photo/Video</h3>
               </div>
               <div className="messageSender__option">
               <InsertEmoticon style={{color:'orange'}} />
               <h3>Feeling/Activity</h3>
               </div>
            </div>
        </div>
    )
}

export default MessageSender
