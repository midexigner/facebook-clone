import { Chat, EmojiFlags,ExpandMore,LocalHospital, People, Storefront, VideoLibrary } from '@material-ui/icons';
import React from 'react'
import { useStateValue } from '../../StateProvider';
import SidebarRow from '../SidebarRow/SidebarRow'
import './Sidebar.css'

const Sidebar = () => {
    const [{user},dispatch] = useStateValue();
    return (
        <div className='sidebar'>
            <SidebarRow src={user.photoURL} title={user.displayName} />
            <SidebarRow Icon={LocalHospital} title="COVID-19 Information Center" />
            <SidebarRow Icon={EmojiFlags} title="Pages" />
            <SidebarRow Icon={People} title="Friends" />
            <SidebarRow Icon={Chat} title="Messengers" />
            <SidebarRow Icon={Storefront} title="Marketplace" />
            <SidebarRow Icon={VideoLibrary} title="Videos" />
            <SidebarRow Icon={ExpandMore} title="More" />
        </div>
    )
}

export default Sidebar
