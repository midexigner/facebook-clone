import React from 'react'
import './App.css';
import Header from './components/Header/Header';
import Sidebar from './components/Sidebar/Sidebar';
import Feed from './components/Feed/Feed';
import Widgets from './components/Widgets/Widgets';
import Login from './components/Login/Login';
import { useStateValue } from './StateProvider'

function App() {
  const [{user},dispatch] = useStateValue();
  console.log(user);
  return (
    <div className="app">
      {!user ? (
      <Login />
      ) :(
      <>
      {/* Header */}
      <Header />
      {/* App Body */}
      <div className="app__body">
      {/* Sidebar */}
      <Sidebar/>
      {/* Feed */}
      <Feed />
      {/* widgets */}
      <Widgets />
      </div>
      </>
      )}
    </div>
  );
}

export default App;
